<?php

class animal
{
    protected $nama; 
    protected $kaki = 4; 
    protected $darahdingin = "no";

    public function __construct($nama)
    {
        $this -> nama = $nama;
    }

    //function get legs
    public function get_kaki()
    {
        return $this -> kaki;
    }

    //function get darah dingin 
    public function get_darahdingin()
    {
        return $this ->darahdingin;
    }

    //function get nama 
    public function get_nama()
    {
        return $this -> nama;
    }
}

